package co.com.sofka.question.services.rest;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class FueEditadoParche implements Question<Boolean> {
    public static final Logger logger = LogManager.getLogger(FueEditadoParche.class);
    private final String parcheEditado;
    private final String parcheConsultado;

    public FueEditadoParche(String parcheEditado, String parcheConsultado) {
        this.parcheEditado = parcheEditado;
        this.parcheConsultado = parcheConsultado;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return parcheEditado.equals(parcheConsultado);
    }

    public static FueEditadoParche seEditoParche (String parcheEditado, String parcheConsultado) {
        return new FueEditadoParche(parcheEditado,parcheConsultado);
    }
}
