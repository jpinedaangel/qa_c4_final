package co.com.sofka.question.services.rest.grupo3;

import co.com.sofka.model.services.rest.grupo3.UserModel;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class RegistroBodyResponse implements Question<UserModel> {

    @Override
    public UserModel answeredBy (Actor actor) {
        return SerenityRest.lastResponse().as(UserModel.class);
    }
}
