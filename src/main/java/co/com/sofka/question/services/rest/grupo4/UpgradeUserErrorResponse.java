package co.com.sofka.question.services.rest.grupo4;

import co.com.sofka.model.services.rest.grupo4.UpgradeUserErrorModel;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class UpgradeUserErrorResponse implements Question<UpgradeUserErrorModel> {

   @Override
   public UpgradeUserErrorModel answeredBy(Actor actor) {
      return SerenityRest.lastResponse().as(UpgradeUserErrorModel.class);
   }
}
