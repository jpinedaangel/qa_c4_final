package co.com.sofka.question.services.rest;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class SeEliminoParche implements Question<Boolean> {
    private Boolean fueEliminadoParche;

    public SeEliminoParche(Boolean fueEliminadoParche) {
        this.fueEliminadoParche = fueEliminadoParche;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return fueEliminadoParche;
    }


    public static SeEliminoParche seEliminoParche (Boolean fueEliminadoParche) {
        return new SeEliminoParche(fueEliminadoParche);
    }
}
