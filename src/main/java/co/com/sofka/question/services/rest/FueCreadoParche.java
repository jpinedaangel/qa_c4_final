package co.com.sofka.question.services.rest;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class FueCreadoParche implements Question<Boolean> {
    public static final Logger logger = LogManager.getLogger(FueCreadoParche.class);
    private final String parcheCreado;
    private final String parcheConsultado;

    public FueCreadoParche(String parcheCreado, String parcheConsultado) {
        this.parcheCreado = parcheCreado;
        this.parcheConsultado=parcheConsultado;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return parcheCreado.equals(parcheConsultado);
    }

    public static FueCreadoParche fueCreadoElParche (String parcheCreado, String parcheConsultado) {
        return new FueCreadoParche(parcheCreado, parcheConsultado);

    }
}
