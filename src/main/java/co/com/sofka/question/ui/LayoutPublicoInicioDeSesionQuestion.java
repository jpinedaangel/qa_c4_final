package co.com.sofka.question.ui;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterfaces.webui.LayoutPublicLocatos.MESSAGE_VALIDATION_INICIO_SESION;


public class LayoutPublicoInicioDeSesionQuestion implements Question <Object>{
    @Override
    public Object answeredBy(Actor actor) {
        return MESSAGE_VALIDATION_INICIO_SESION.resolveFor(actor).getText();
    }
    public static LayoutPublicoInicioDeSesionQuestion layoutPublicoInicioDeSesionQuestion(){
        return new LayoutPublicoInicioDeSesionQuestion();
    }
}
