package co.com.sofka.question.ui;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterfaces.webui.LayoutPrivadoLocators.MESSAGE_VALIDATION_INICIO_PRIVADO;

public class LayoutPrivadoInicioQuestion implements Question <Object>{
    @Override
    public Object answeredBy(Actor actor) {
        return MESSAGE_VALIDATION_INICIO_PRIVADO.resolveFor(actor).getText();
    }
    public static LayoutPrivadoInicioQuestion layoutPrivadoInicioQuestion(){
        return new LayoutPrivadoInicioQuestion();
    }
}
