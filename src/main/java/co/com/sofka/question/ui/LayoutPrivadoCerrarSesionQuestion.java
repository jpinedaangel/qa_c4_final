package co.com.sofka.question.ui;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterfaces.webui.LayoutPrivadoLocators.MESSAGE_VALIDATION_CERRAR_SESION;

public class LayoutPrivadoCerrarSesionQuestion implements Question <Object>{
    @Override
    public Object answeredBy(Actor actor) {
        return MESSAGE_VALIDATION_CERRAR_SESION.resolveFor(actor).getText();
    }
    public static LayoutPrivadoCerrarSesionQuestion layoutPrivadoCerrarSesionQuestion(){
        return new LayoutPrivadoCerrarSesionQuestion();
    }
}
