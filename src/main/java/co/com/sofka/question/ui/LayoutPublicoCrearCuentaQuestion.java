package co.com.sofka.question.ui;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterfaces.webui.LayoutPublicLocatos.MESSAGE_VALIDATION_CREAR_CUENTA;


public class LayoutPublicoCrearCuentaQuestion implements Question <Object>{
    @Override
    public Object answeredBy(Actor actor) {
        return MESSAGE_VALIDATION_CREAR_CUENTA.resolveFor(actor).getText();
    }
    public static LayoutPublicoCrearCuentaQuestion layoutPublicoCrearCuentaQuestion(){
        return new LayoutPublicoCrearCuentaQuestion();
    }
}
