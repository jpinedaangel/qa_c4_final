package co.com.sofka.question.ui;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterfaces.webui.LayoutPrivadoLocators.MESSAGE_VALIDATION_MIS_PARCHES;

public class LayoutPrivadoMisParchesQuestion implements Question <Object>{
    @Override
    public Object answeredBy(Actor actor) {
        return MESSAGE_VALIDATION_MIS_PARCHES.resolveFor(actor).getText();
    }
    public static LayoutPrivadoMisParchesQuestion layoutPrivadoMisParchesQuestion(){
        return new LayoutPrivadoMisParchesQuestion();
    }
}
