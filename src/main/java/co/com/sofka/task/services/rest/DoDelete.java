package co.com.sofka.task.services.rest;

import co.com.sofka.question.services.rest.grupo4.ObtenerIdComentario;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Delete;

public class DoDelete implements Task {

    private String resource;


    public DoDelete withTheResource(String resource){
        this.resource = resource;
        return this;
    }


    @Override
    public <T extends Actor> void performAs(T actor) {

        String id = actor.asksFor(ObtenerIdComentario.was());

        actor.attemptsTo(
                Delete.from(resource)
                        .with(request -> request.pathParam("id",id))
        );
    }

    public static DoDelete doDelete(){
        return new DoDelete();
    }
}
