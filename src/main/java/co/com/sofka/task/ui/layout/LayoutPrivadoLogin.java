package co.com.sofka.task.ui.layout;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static co.com.sofka.userinterfaces.webui.LayoutPrivadoLocators.*;

public class LayoutPrivadoLogin implements Task {

    private String email;
    private String name;
    private String password;

    public LayoutPrivadoLogin withEmail(String email) {
        this.email = email;
        return this;
    }

    public LayoutPrivadoLogin withName(String name) {
        this.name = name;
        return this;
    }

    public LayoutPrivadoLogin andPassword(String password) {
        this.password = password;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(CREAR_CUENTA_PRIVADO),
                Enter.theValue(name).into(NAME_USER),
                Enter.theValue(email).into(EMAIL_USER),
                Enter.theValue(password).into(PASSWORD_USER),
                Enter.theValue(password).into(PASSWORD_USER_2),
                Click.on(BOTON_CREAR_CUENTA)
        );
    }

    public static LayoutPrivadoLogin layoutPrivadoLogin(){
        return new LayoutPrivadoLogin();
    }
}
