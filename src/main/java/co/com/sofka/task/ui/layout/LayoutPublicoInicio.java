package co.com.sofka.task.ui.layout;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.sofka.userinterfaces.webui.LayoutPublicLocatos.INICIO;

public class LayoutPublicoInicio implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(INICIO)
        );
    }

    public static LayoutPublicoInicio layoutPublicoInicio(){
        return new LayoutPublicoInicio();
    }
}
