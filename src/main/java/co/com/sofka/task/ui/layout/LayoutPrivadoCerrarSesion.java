package co.com.sofka.task.ui.layout;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.sofka.userinterfaces.webui.LayoutPrivadoLocators.CERRAR_SESION;

public class LayoutPrivadoCerrarSesion implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(CERRAR_SESION)
        );
    }

    public static LayoutPrivadoCerrarSesion layoutPrivadoCerrarSesion(){
        return new LayoutPrivadoCerrarSesion();
    }
}
