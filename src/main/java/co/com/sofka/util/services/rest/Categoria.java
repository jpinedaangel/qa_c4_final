package co.com.sofka.util.services.rest;

import com.github.javafaker.Faker;

public enum Categoria {
    TECNOLOGIA,
    VIDEOJUEGOS,
    ARTE,
    NEGOCIOS,
    MODA,
    DEPORTE,
    GASTRONOMIA,
    FIESTAS,
    CONFERENCIAS,
    CITA,
    LECTURA,
    APRENDIZAJE,
    VARIOS;

    public static String seleccionarCategoria(){
        String [] categorias = new String[13];
        Faker faker = new Faker();

        categorias[0] =  TECNOLOGIA.name();
        categorias[1] =  VIDEOJUEGOS.name();
        categorias[2] =  ARTE.name();
        categorias[3] =  NEGOCIOS.name();
        categorias[4] =  MODA.name();
        categorias[5] =  DEPORTE.name();
        categorias[6] =  GASTRONOMIA.name();
        categorias[7] =  FIESTAS.name();
        categorias[8] =  CONFERENCIAS.name();
        categorias[9] =  CITA.name();
        categorias[10]=  LECTURA.name();
        categorias[11]=  APRENDIZAJE.name();
        categorias[12]=  VARIOS.name();
        return categorias[ faker.number().numberBetween(1,14)-1];
    }
}
