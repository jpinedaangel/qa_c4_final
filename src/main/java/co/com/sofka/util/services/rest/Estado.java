package co.com.sofka.util.services.rest;

import com.github.javafaker.Faker;

public enum Estado {
    HABILITADO,
    DESHABILITADO;

    public static String seleccionarEstado(){
        Faker faker =new Faker();
        int value = faker.number().numberBetween(1,3);

        if (value == 1) {
            return HABILITADO.name();

        } else {
            return DESHABILITADO.name();
        }
    }
}
