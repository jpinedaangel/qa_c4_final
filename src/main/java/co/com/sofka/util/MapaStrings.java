package co.com.sofka.util;

public enum MapaStrings {
    COMILLA_COMA("\",\n"),
    COMILLA_COMA_LLAVE("\"},\n"),
    SALTO_LINEA("\n"),
    COMA_SALTO_LINEA(",\n"),
    LLAVE_COMA_SALTO_LINEA("},\n");

    private final String value;

    MapaStrings(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
