package co.com.sofka.util;

public class Utilities {

    private Utilities() {
    }

    public static String getUserDir(){
        return System.getProperty("user.dir");
    }

    public static String defineOS(){
        return System.getProperty("os.name");
    }

    public static String osPathModify(String os, String path){
        if(isMac(os) && path.contains("\\")){
            return path.replace("\\", "/");
        } else if (isWindows(os) && path.contains("/")){
            return path.replace("/", "\\");
        } else if (isLinux(os) && path.contains("\\")){
            return path.replace("\\", "/");
        }
        return path;
    }

    private static boolean isWindows(String os){
        return os.contains("Windows");
    }

    private static boolean isMac(String os){
        return os.contains("Mac OS");
    }

    private static boolean isLinux(String os){
        return os.contains("nix") || os.contains("nux") || os.contains("aix");
    }

}
