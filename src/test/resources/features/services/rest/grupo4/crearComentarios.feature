#language: es

Característica: Comentarios dentro de un parche
  Como usuario autenticado quiero poder crear comentarios dentro del parche para socializar con otros usuarios.

  Antecedentes:
    Dado que el usuario tiene acceso a la aplicación parche

@CrearComentario
  Esquema del escenario: Envío exitoso de un comentario
    Cuando el usuario con id "<userId>" envía un "<mensaje>" en el parche con id "<parcheId>"
    Entonces obtiene en la petición un código de respuesta exitoso en el envío del comentario
    Y puede ver en el campo comentario el "<mensaje>" realizado
    Y visualiza en el campo usuario el nombre del responsable del comentario
    Y en el campo fechaCreacion la fecha de creación del comentario

    Ejemplos:
      |           userId              |           mensaje                  |    parcheId              |
      | CfVNRJehPJOe7F0MUgnhLWz68qz2  | Este es otro comentario de prueba1 | 61fcaf90e67b3835150b05cc |

  @EnvíoComentarioFallido
  Esquema del escenario: Envío fallido de un comentario con userId no existente o con parcheId no existente
    Cuando el usuario con id "<userId>" envía un "<mensaje>" en el parche con id "<parcheId>"
    Entonces obtiene en la petición un código de respuesta de error Not Found

    Ejemplos:
      |         userId                 |            mensaje                |        parcheId            |
      | user61fc3914785823535471       | Este es otro comentario de prueba |  61fcaf90e67b3835150b05cc  |
      | CfVNRJehPJOe7F0MUgnhLWz68qz2   | Este es otro comentario de prueba |  parch48dbe67b3835150b04c4 |


