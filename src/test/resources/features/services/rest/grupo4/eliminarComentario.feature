#language: es

Característica: Eliminar comentario

  Antecedentes:

    Dado que el usuario tiene acceso a la aplicación parche

  @EliminarComentario
  Esquema del escenario: Eliminación exitosa de un comentario por usuario dueño del parche
      Cuando el usuario con id "<userId>" envía un "<mensaje>" en el parche con id "<parcheId>"
      Y el usuario elimina el comentario creado
      Entonces obtiene en la petición un código de respuesta exitoso en la eliminación del comentario.
      Y puede ver el mensaje "Eliminado"

    Ejemplos:
      |           userId              |           mensaje                  |    parcheId              |
      | CfVNRJehPJOe7F0MUgnhLWz68qz2  | Este es otro comentario de prueba1 | 61fcaf90e67b3835150b05cc |

