#language: es

Característica: Desabilitar parche
           Yo como usuario autenticado dueño de un parche
           quiero deshabilitar el parche
           para que las personas no pierdan el tiempo yendo a ciertos lugares debido a cancelaciones o novedades.



 @deshabilitarParche
Escenario: desabilitar parche
   Cuando el usuario ejecute la peticion para desabilitar parche
   Entonces el estado debe cambiar a DESHABILITADO