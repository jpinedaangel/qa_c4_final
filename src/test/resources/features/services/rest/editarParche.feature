#language: es

  Característica: Editar parche
    Yo como creador del parche
    quiero editar el formulario
    para corregir errores o novedades

  @editarParche
  Escenario: editar parche
    Cuando el usuario envie la peticion con los datos nuevos
    Entonces el parche deberá ser actualizado en la base de datos