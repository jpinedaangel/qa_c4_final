#language: es
Característica: Inicio de sesión
  Yo como usuario quiero ingresar a la plataforma para poder vivir
  la experiencia con los servicios que ella presta.
  Antecedentes:
  Dado el usuario se encuentra en la página de inicio de sesión

  Escenario: Inicio de sesión exitoso
  Cuando el usuario envía la petición de inicio de sesión con el UID
  Entonces el usuario recibe un código de respuesta OK con la información del usuario.

  Escenario: Inicio de sesión fallido
  Cuando el usuario envía la petición de inicio de sesión con UID no registrado
  Entonces el usuario recibe un código de respuesta CONFLICT y un mensaje de error “conflict”