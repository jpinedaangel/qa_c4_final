#language: es
Característica: Registro de Usuario
  Yo como usuario quiero un formulario donde me permita
  ingresar mis datos para el ingreso a la plataforma.

  Antecedentes:
    Dado el usuario se encuentra en la página de registro

  Escenario: registro usuario exitoso
    Cuando el usuario envía la petición de registro con los campos solicitados
    Entonces el usuario recibe un código de respuesta OK con la información registrada

  Escenario: Registro de usuario fallido
    Cuando el usuario envía la petición de registro con email ya registrado
    Entonces el usuario recibe un código de respuesta CONFLICT y un mensaje “conflict” en el campo error
