#language: es

Característica: Prueba HU008, HU012, HU013

    Yo como usuario autenticado
    Quiero poder visualizar la lista de parches, la lista de mis parches y el detalle de los mismos
    para tener un seguimiento de los parches activos.

    @listarParches
    Escenario: Listar parches
        Dado que el usuario quiere listar los parches
        Cuando el usuario filtra por nombre
        Entonces el usuario deber ver una lista de los parches por nombres

    @listarMisParches
    Escenario: Listar mis parches
        Dado que el usuario administrador requiere validar sus parches
        Cuando el usuario ingresa a la seccion de mis parches
        Entonces deberia poder ver la respuesta del servicio y estado no nulo del id del parche


    @detalleParches
    Escenario: Detalle del Parche
        Dado que el usuario administrador requiere verificar los detalles del parche
        Cuando el usuario ingresa a verificar un parche creado
        Entonces  deberia poder visualizar la respuesta del servicio y estado no nulo del id del parche