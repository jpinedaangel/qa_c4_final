package co.com.sofka.stepdefinitions.services.rest;

import co.com.sofka.SetupRest;
import co.com.sofka.database.mongodb.MongodbCrud;
import co.com.sofka.model.services.rest.Parche;
import co.com.sofka.util.services.rest.Estado;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Entonces;
import org.apache.http.HttpStatus;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;
import org.bson.Document;
import org.bson.types.ObjectId;

import static co.com.sofka.database.mongodb.MongodbCrud.crearMongoCrud;
import static co.com.sofka.question.services.rest.FueDesabilitadoParche.seDesabilitoParche;
import static co.com.sofka.task.services.rest.DoPut.doPut;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;


public class DesHabilitarParcheStepdefinition extends SetupRest {
    private final Parche parche = Parche.generarParche();
    private final String idParche =obtenerIdParcheHabilitado();
    private static final Logger logger = LogManager.getLogger(DesHabilitarParcheStepdefinition.class);

    @Cuando("el usuario ejecute la peticion para desabilitar parche")
    public void elUsuarioEjecuteLaPeticionParaDesabilitarParche() {
        parche.setEstado(Estado.DESHABILITADO.name());
        try {
            super.setupRest();
            actor.attemptsTo(
                      doPut()
                            .withTheResource(DESHABILITAR_PARCHE)
                            .andTheBodyRequest(String.format(parche.parcheRequestEditar(), idParche))

            );
        } catch (Exception e) {
            logger.warn("error al ejecutar petición\n"+e);
            Assertions.fail(e.getMessage());
        }
    }
    @Entonces("el estado debe cambiar a DESHABILITADO")
    public void elEstadoDebeCambiarAHabilitar() {
        try {
            actor.should(
                    seeThatResponse(
                            resp-> {
                                resp.statusCode(HttpStatus.SC_OK);
                                logger.info("codigo de respuesta: "+resp.extract().statusCode());
                            }
                    ),
                    seeThat(
                            seDesabilitoParche(obtenerEstadoParche())
                    ));
            logger.info("parche fue desabilitado exitosamente");
        } catch (AssertionError e) {
            logger.warn("Error en la validación\n" + e);
            Assertions.fail(e.getMessage());
        }
    }
    protected String obtenerIdParcheHabilitado() {
        MongodbCrud mongoDataBase =crearMongoCrud();

        MongoCollection<Document> parchesHAbilitados = mongoDataBase.getAllDocument("parches");
        Document docParcheHabilitado = parchesHAbilitados.find(new Document().append("estado","HABILITADO")).first();

        if (docParcheHabilitado != null) {
            return String.valueOf(docParcheHabilitado.getObjectId("_id"));
        }else{
            return "";
        }
    }
    protected String obtenerEstadoParche(){
        MongodbCrud mongoDataBase =crearMongoCrud();

        FindIterable<Document> parches = mongoDataBase.getAllDocument("parches").find(
                new Document().append("estado","DESHABILITADO"));

        Document parcheConsultado = parches.filter(new Document().append("_id",new ObjectId(idParche))).first();
        mongoDataBase.closeConnetions();
        if (parcheConsultado != null) {
            return parcheConsultado.getString("estado");
        }else{
            return "";
        }
    }
}
