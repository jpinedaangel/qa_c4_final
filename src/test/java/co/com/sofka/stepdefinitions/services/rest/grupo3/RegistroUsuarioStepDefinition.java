package co.com.sofka.stepdefinitions.services.rest.grupo3;

import co.com.sofka.SetupRest;
import co.com.sofka.model.services.rest.grupo3.UserModel;
import co.com.sofka.question.services.rest.ResponseCode;
import co.com.sofka.question.services.rest.grupo3.ErrorBodyResponse;
import co.com.sofka.question.services.rest.grupo3.RegistroBodyResponse;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;

import static co.com.sofka.task.services.rest.DoPost.doPost;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

public class RegistroUsuarioStepDefinition extends SetupRest {
    public static Logger LOGGER = Logger.getLogger(RegistroUsuarioStepDefinition.class);
    private UserModel userModel;

    @Dado("el usuario se encuentra en la página de registro")
    public void elUsuarioSeEncuentraEnLaPaginaDeRegistro () {
        try {
            super.setupRest();
            successUser();
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
Assertions.fail("");
        }
    }

    @Cuando("el usuario envía la petición de registro con los campos solicitados")
    public void elUsuarioEnviaLaPeticionDeRegistroConLosCamposSolicitados () {
        try {
            actor.attemptsTo(doPost()
                    .withTheResource(RESOURCE_REGISTRO)
                    .andTheBodyRequest(userModel)
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail("");
        }
    }

    @Entonces("el usuario recibe un código de respuesta OK con la información registrada")
    public void elUsuarioRecibeUnCodigoDeRespuestaOKConLaInformacionRegistrada () {
        try {
            UserModel actualResponse = new RegistroBodyResponse().answeredBy(actor);
            actor.should(
                    seeThat("El codigo de respuesta debe ser" + HttpStatus.SC_OK, ResponseCode
                            .was(), equalTo(HttpStatus.SC_OK)),
                    seeThat("El UID registrado debe ser " + userModel.getUid(),
                            datos -> actualResponse.getUid(),
                            equalTo(userModel.getUid())),
                    seeThat("El nombre registrado debe ser " + userModel.getNombres(),
                            datos -> actualResponse.getNombres(),
                            equalTo(userModel.getNombres()))
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail("");
        }

    }


    @Cuando("el usuario envía la petición de registro con email ya registrado")
    public void elUsuarioEnvíaLaPeticionDeRegistroConEmailYaRegistrado () {
        try {
            actor.attemptsTo(doPost()
                    .withTheResource(RESOURCE_REGISTRO)
                    .andTheBodyRequest(userModel)
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail("");
        }

    }

    @Entonces("el usuario recibe un código de respuesta CONFLICT y un mensaje “conflict” en el campo error")
    public void elUsuarioRecibeUnCodigoDeRespuestaCONFLICTYUnMensajeConflictEnElCampoError () {
        try {
            actor.should(
                    seeThat("El codigo de respuesta debe ser " + HttpStatus.SC_CONFLICT, ResponseCode
                            .was(), equalTo(HttpStatus.SC_CONFLICT)),
                    seeThat("El registro creado debe ser 'Conflict'",
                            datos -> new ErrorBodyResponse().answeredBy(actor).getError(),
                            equalTo("Conflict"))
            );
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail("");
        }
    }

    private UserModel successUser () {
        userModel = new UserModel();
        userModel.setUid("uAQrYwji8oZqacgLU2LZkZANZPS2");
        userModel.setNombres("Brahian Rodriguez");
        userModel.setEmail("meris@gmail.com");
        userModel.setImagenUrl("https://firebasestorage.googleapis.com/v0/b/quetions-app.appspot.com/o/pngwing.com.png?alt=media&token=ae687cb3-1160-4aa6-909c-a4e320f0a1c6");
        return userModel;
    }
}
