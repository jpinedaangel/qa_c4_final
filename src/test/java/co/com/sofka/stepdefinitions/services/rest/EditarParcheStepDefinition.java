package co.com.sofka.stepdefinitions.services.rest;

import co.com.sofka.SetupRest;
import co.com.sofka.database.mongodb.MongodbCrud;
import co.com.sofka.model.services.rest.Parche;
import com.mongodb.client.MongoCollection;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Entonces;
import org.apache.http.HttpStatus;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;
import org.bson.Document;
import org.bson.types.ObjectId;

import static co.com.sofka.database.mongodb.MongodbCrud.crearMongoCrud;
import static co.com.sofka.question.services.rest.FueEditadoParche.seEditoParche;
import static co.com.sofka.task.services.rest.DoPut.doPut;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;


public class EditarParcheStepDefinition extends SetupRest {
    private final Parche parche = Parche.generarParche();
    private final String idParche = obtenerIdParche();
    private static final Logger logger = LogManager.getLogger(EditarParcheStepDefinition.class);


    @Cuando("el usuario envie la peticion con los datos nuevos")
    public void elUsuarioEnvieLaPeticionConLosDatosNuevos() {
        try {
            super.setupRest();
            actor.attemptsTo(
                    doPut()
                            .withTheResource(EDITAR_PARCHE)
                            .andTheBodyRequest(String.format(parche.parcheRequestEditar(), idParche))
            );
        } catch (Exception e) {
            logger.warn("error al ejecutar petición\n"+e);
            Assertions.fail(e.getMessage());
        }
    }

    @Entonces("el parche deberá ser actualizado en la base de datos")
    public void elParcheDeberaSerActualizadoEnLaBaseDeDatos() {
        try {
            actor.should(
                    seeThatResponse(
                            resp-> {
                                resp.statusCode(HttpStatus.SC_OK);
                                logger.info("codigo de respuesta: "+resp.extract().statusCode());
                            }
                    ),seeThat(
                            seEditoParche(
                                    parche.parcheResponseMongodb(),
                                    consultarParcheEditado()
                            )
                    )
                    );
            logger.info("parche modificado en la base de datos");
        } catch (AssertionError e) {
            logger.warn("Error en la validación\n" + e);
            Assertions.fail(e.getMessage());
        }
    }

    protected String obtenerIdParche(){
        MongodbCrud mongoDataBase =crearMongoCrud();

        MongoCollection<Document> parchesHAbilitados = mongoDataBase.getAllDocument("parches");
        Document docParcheHabilitado = parchesHAbilitados.find(new Document().append("estado","DESHABILITADO")).first();
        mongoDataBase.closeConnetions();
        if (docParcheHabilitado != null) {
            return String.valueOf(docParcheHabilitado.getObjectId("_id"));
        }else{
            return "";
        }
    }
    protected String consultarParcheEditado(){
        String parcheFormat="";
        MongodbCrud mongoDataBase =crearMongoCrud();

        ObjectId id = new ObjectId(idParche);
        MongoCollection<Document> parches = mongoDataBase.getAllDocument("parches");

        Document doc = parches.find(new Document().append("_id",id)).first();

        if (doc != null) {
            parcheFormat = Parche.consultarParche(doc);
        }
        mongoDataBase.closeConnetions();

        return parcheFormat;
    }
}
