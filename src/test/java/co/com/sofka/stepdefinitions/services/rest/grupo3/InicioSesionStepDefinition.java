package co.com.sofka.stepdefinitions.services.rest.grupo3;

import co.com.sofka.SetupRest;
import co.com.sofka.model.services.rest.grupo3.UserModel;
import co.com.sofka.question.services.rest.ResponseCode;
import co.com.sofka.question.services.rest.grupo3.ErrorBodyResponse;
import co.com.sofka.question.services.rest.grupo3.RegistroBodyResponse;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;

import static co.com.sofka.task.services.rest.DoGetInicioSesion.doGetInicioSesion;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

public class InicioSesionStepDefinition extends SetupRest {
    public static Logger LOGGER = Logger.getLogger(InicioSesionStepDefinition.class);
    private UserModel userModel;

    @Dado("el usuario se encuentra en la página de inicio de sesión")
    public void elUsuarioSeEncuentraEnLaPáginaDeInicioDeSesión () {
        try {
            super.setupRest();
            registeredUser();
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail("");
        }
    }

    @Cuando("el usuario envía la petición de inicio de sesión con el UID")
    public void elUsuarioEnvíaLaPeticiónDeInicioDeSesiónConElUid () {
        try {
            actor.attemptsTo(
                    doGetInicioSesion()
                            .withTheResource(String.format(RESOURCE_INICIO_SESION, userModel.getUid()))
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail("");
        }
    }

    @Entonces("el usuario recibe un código de respuesta OK con la información del usuario.")
    public void elUsuarioRecibeUnCódigoDeRespuestaOKConLaInformaciónDelUsuario () {
        try {
            UserModel actualResponse = new RegistroBodyResponse().answeredBy(actor);
            actor.should(
                    seeThat("El codigo de respuesta debe ser " + HttpStatus.SC_OK, ResponseCode
                            .was(), equalTo(HttpStatus.SC_OK)),
                    seeThat("El UID del inicio de sesión debe ser: " + userModel.getUid(),
                            datos -> actualResponse.getUid(),
                            equalTo(userModel.getUid())),
                    seeThat("El email autorizado debe ser " + userModel.getEmail(),
                            datos -> actualResponse.getEmail(),
                            equalTo(userModel.getEmail()))
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail("");
        }
    }


    @Cuando("el usuario envía la petición de inicio de sesión con UID no registrado")
    public void elUsuarioEnvíaLaPeticiónDeInicioDeSesiónConUidNoRegistrado () {
        try {
            actor.attemptsTo(
                    doGetInicioSesion()
                            .withTheResource(String.format(RESOURCE_INICIO_SESION, "61f93e36040edb346512eea5"))
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail("");
        }
    }

    @Entonces("el usuario recibe un código de respuesta CONFLICT y un mensaje de error “conflict”")
    public void elUsuarioRecibeUnCódigoDeRespuestaCONFLICTYUnMensajeDeErrorConflict () {
        try {
            actor.should(
                    seeThat("El codigo de respuesta debe ser " + HttpStatus.SC_UNAUTHORIZED, ResponseCode
                            .was(), equalTo(HttpStatus.SC_UNAUTHORIZED)),
                    seeThat("El mensaje de error debe ser 'Unauthorized'",
                            datos -> new ErrorBodyResponse().answeredBy(actor).getError(),
                            equalTo("Unauthorized"))
            );
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail("");
        }
    }

    private UserModel registeredUser () {
        userModel = new UserModel();
        userModel.setUid("uAQrYwji8oZqacgLU2LZkZANZPS2");
        userModel.setNombres("Brahian Rodriguez");
        userModel.setEmail("meris@gmail.com");
        userModel.setImagenUrl("https://firebasestorage.googleapis.com/v0/b/quetions-app.appspot.com/o/pngwing.com.png?alt=media&token=ae687cb3-1160-4aa6-909c-a4e320f0a1c6");
        return userModel;

    }
}
