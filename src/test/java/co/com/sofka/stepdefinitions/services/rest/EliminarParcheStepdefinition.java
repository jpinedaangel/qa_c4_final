package co.com.sofka.stepdefinitions.services.rest;

import co.com.sofka.SetupRest;
import co.com.sofka.database.mongodb.MongodbCrud;
import com.mongodb.client.MongoCollection;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Entonces;
import org.apache.http.HttpStatus;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;
import org.bson.Document;
import org.bson.types.ObjectId;

import static co.com.sofka.database.mongodb.MongodbCrud.crearMongoCrud;
import static co.com.sofka.question.services.rest.SeEliminoParche.seEliminoParche;
import static co.com.sofka.task.services.rest.DoDeleteParche.doDeleteParche;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;


public class EliminarParcheStepdefinition extends SetupRest {
    private static final Logger logger = LogManager.getLogger(CrearParcheNombreInvalidoStepdefinition.class);
    private String idParcheEliminar;
    @Cuando("el usuario ejecute la peticion para eliminar un parche")
    public void elUsuarioEjecuteLaPeticionParaEliminarUnParche() {
        idParcheEliminar = idParcheEliminar();
        try {
            super.setupRest();
            actor.attemptsTo(
                    doDeleteParche()
                            .withTheResource(ELIMINAR_PARCHE+idParcheEliminar)
                            .andTheBodyRequest("")
            );
        } catch (Exception e) {
            logger.warn("error al ejecutar petición\n"+e);
            Assertions.fail(e.getMessage());
        }
    }

    @Entonces("el parche debe eliminarse de la base de datos")
    public void elParcheDebeEliminarseDeLaBaseDeDatos() {
        try {
            actor.should(
                    seeThatResponse(
                            resp-> {
                                resp.statusCode(HttpStatus.SC_OK);
                                logger.info("codigo de respuesta: "+resp.extract().statusCode());
                            }
                    ),
                    seeThat(
                            seEliminoParche(consultarParcheELiminado())
                    ));
            logger.info("parche eliminado exitosamente, no se encontró en la base de datos");
        } catch (AssertionError e) {
            logger.warn("Error en la validación\n" + e);
            Assertions.fail(e.getMessage());
        }
    }
    protected String idParcheEliminar(){
        MongodbCrud mongoDataBase =crearMongoCrud();

        Document parche= mongoDataBase.getDocumentColletion("parches");

        mongoDataBase.closeConnetions();

        return parche.getObjectId("_id").toString();
    }
    protected Boolean consultarParcheELiminado(){
        MongodbCrud mongoDataBase =crearMongoCrud();


        ObjectId id = new ObjectId(idParcheEliminar);
        MongoCollection<Document> parches = mongoDataBase.getAllDocument("parches");
        return parches.find(new Document().append("_id",id)).first()==null;
    }
}