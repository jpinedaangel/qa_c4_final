package co.com.sofka.stepdefinitions.services.rest.grupo4;

import co.com.sofka.SetupRest;
import co.com.sofka.model.services.rest.grupo4.Comentario;
import co.com.sofka.question.services.rest.ResponseCode;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.cucumber.java.es.Y;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;

import static co.com.sofka.task.services.rest.DoDelete.doDelete;
import static co.com.sofka.task.services.rest.DoPost.doPost;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.*;

public class ComentariosStepdefinition extends SetupRest {

    protected static final Logger LOGGER = Logger.getLogger(ComentariosStepdefinition.class);


    @Dado("que el usuario tiene acceso a la aplicación parche")
    public void queElUsuarioTieneAccesoALaAplicacionParche() {
        try{
            super.setupRest();
            LOGGER.info("Se pudo acceder a la aplicación");
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            Assertions.fail("");
        }
    }

    @Cuando("el usuario con id {string} envía un {string} en el parche con id {string}")
    public void elUsuarioComentarioConIdEnviaUnEnElParcheConId(String userId, String comentario, String parcheId) {
        try {
            Comentario comentarioUsuario = new Comentario();
            comentarioUsuario.setUserId(userId);
            comentarioUsuario.setParcheId(parcheId);
            comentarioUsuario.setMensaje(comentario);

            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE_CREAR_COMENTARIO)
                            .andTheBodyRequest(comentarioUsuario)
            );
            LOGGER.info("Se pudo realizar la petición");
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            Assertions.fail("");
        }
    }

    @Entonces("obtiene en la petición un código de respuesta exitoso en el envío del comentario")
    public void obtieneEnLaPeticionUnCodigoDeRespuestaExitosoEnElEnvioDelComentario() {
        try{
            actor.should(
                    seeThat("El código de respuesta HTTP debe ser: " + HttpStatus.SC_OK,
                            ResponseCode.was(), equalTo(HttpStatus.SC_OK))
            );
            LOGGER.info("Se obtuvo el código de respuesta esperado");
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            Assertions.fail("");
        }
    }

    @Y("puede ver en el campo comentario el {string} realizado")
    public void puedeVerEnElCampoElComentarioRealizado(String comentario) {
        try{
            actor.should(
                    seeThatResponse("El valor en la respuesta debe ser: " + comentario,
                            response -> response.body("mensaje", equalTo(comentario)))
            );
            LOGGER.info("Obtuvo el mensaje esperado");

        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            Assertions.fail("");
        }
    }
    @Entonces("visualiza en el campo usuario el nombre del responsable del comentario")
    public void visualizaEnElCampoElNombreDelResponsableDelComentario() {

        try{
            actor.should(
                    seeThatResponse("El valor en la respuesta debe ser: ",
                            response -> response.body("usuario", notNullValue()))
            );
            LOGGER.info("Obtuvo el resultado esperado");

        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            Assertions.fail("");
        }
    }

    @Y("en el campo fechaCreacion la fecha de creación del comentario")
    public void enElCampoLaFechaDeCreacionDelComentario() {
        try{
            actor.should(
                    seeThatResponse("El valor en la respuesta debe ser: ",
                            response -> response.body("fechaCreacion", notNullValue()))
            );
            LOGGER.info("Obtuvo el resultado esperado");

        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            Assertions.fail("");
        }
    }

    @Entonces("obtiene en la petición un código de respuesta de error Not Found")
    public void obtieneEnLaPeticionUnCodigoDeRespuestaDeErrorNotFound() {
        try{
            actor.should(
                    seeThat("El código de respuesta HTTP debe ser: " + HttpStatus.SC_BAD_REQUEST,
                            ResponseCode.was(), equalTo(HttpStatus.SC_BAD_REQUEST))
            );
            LOGGER.info("Se obtuvo el código de respuesta esperado");
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            Assertions.fail("");
        }
    }

    @Cuando("el usuario elimina el comentario creado")
    public void elUsuarioConIdEliminaUnComentarioConId() {
        try {
            actor.attemptsTo(
                    doDelete()
                            .withTheResource(RESOURCE_ELIMINAR_COMENTARIO)
            );
            LOGGER.info("Se pudo realizar la petición");
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            Assertions.fail("");
        }
    }

    @Entonces("obtiene en la petición un código de respuesta exitoso en la eliminación del comentario.")
    public void obtieneEnLaPeticionUnCdigoDeRespuestaExitoso() {
        try{
            actor.should(
                    seeThat("El código de respuesta HTTP debe ser: " + HttpStatus.SC_OK,
                            ResponseCode.was(), equalTo(HttpStatus.SC_OK))
            );
            LOGGER.info("Se obtuvo el código de respuesta esperado");
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            Assertions.fail("");
        }
    }

    @Y("puede ver el mensaje {string}")
    public void puedeVerEnElCampo(String mensaje) {
        try{
            actor.should(
                    seeThatResponse("El valor en la respuesta debe ser: " + mensaje,
                            response -> response.body(equalTo(mensaje)))
            );
            LOGGER.info("Obtuvo el mensaje esperado");

        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            Assertions.fail("");
        }
    }

    @Entonces("obtiene en la petición un código de respuesta de error bad request.")
    public void obtieneEnLaPeticionUnCodigoDeRespuestaDeError() {
        try{
            actor.should(
                    seeThat("El código de respuesta HTTP debe ser: " + HttpStatus.SC_BAD_REQUEST,
                            ResponseCode.was(), equalTo(HttpStatus.SC_BAD_REQUEST))
            );
            LOGGER.info("Se obtuvo el código de respuesta esperado");
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            Assertions.fail("");
        }
    }

}
