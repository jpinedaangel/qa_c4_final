package co.com.sofka.stepdefinitions.services.rest;

import co.com.sofka.SetupRest;
import co.com.sofka.model.services.rest.grupo5.ListData;
import co.com.sofka.question.services.rest.ResponseCode;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import static co.com.sofka.task.services.rest.Doget.doGet;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
public class Grupo5stepdefinition extends SetupRest {

    public static Logger LOGGER = Logger.getLogger(Grupo5stepdefinition.class);

    @Dado("que el usuario quiere listar los parches")
    public void queElUsuarioQuiereListarLosParches() {

        try {
            super.setupRest();
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }

}

    @Cuando("el usuario filtra por nombre")
    public void elUsuarioFiltraPorNombre() {

        try {
            actor.attemptsTo(
                    doGet()
                            .withTheResource(RESOURCE_LISTA)
            );

        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }

    }

    @Entonces("el usuario deber ver una lista de los parches por nombres")
    public void elUsuarioDeberVerUnaListaDeLosParchesPorNombres() {

        ListData userResponse = new ListData();

        try {
            actor.should(
                    seeThat("El codigo de respuesta",
                            ResponseCode
                                    .was(), equalTo(HttpStatus.SC_OK))
            );

        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }

    }

    @Dado("que el usuario administrador requiere validar sus parches")
    public void queElUsuarioAdministradorRequiereValidarSusParches() {
        try {
            super.setupRest();
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }

    @Cuando("el usuario ingresa a la seccion de mis parches")
    public void elUsuarioIngresaALaSeccionDeMisParches() {
        try {
            actor.attemptsTo(
                    doGet()
                            .withTheResource(RESOURCE_MIS_PARCHES)
            );

        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }
    @Entonces("deberia poder ver la respuesta del servicio y estado no nulo del id del parche")
    public void deberiaPoderVerLaRespuestaDelServicioYEstadoNoNuloDelIdDelParche() {

        ListData userResponse = new ListData();

        try {
            actor.should(
                    seeThat("El codigo de respuesta",
                            ResponseCode
                                    .was(), equalTo(HttpStatus.SC_OK))
            );

            actor.should(
                    seeThat("El Id de usuario no debe ser nulo",
                            act -> userResponse.getId(), notNullValue())
            );

        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }

    @Dado("que el usuario administrador requiere verificar los detalles del parche")
    public void queElUsuarioAdministradorRequiereVerificarLosDetallesDelParche() {
        try {
            super.setupRest();
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }

    @Cuando("el usuario ingresa a verificar un parche creado")
    public void elUsuarioIngresaAVerificarUnParcheCreado() {
        try {
            actor.attemptsTo(
                    doGet()
                            .withTheResource(RESOURCE_DETALLE_PARCHES)
            );

        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }
    @Entonces("deberia poder visualizar la respuesta del servicio y estado no nulo del id del parche")
    public void deberiaPoderVisualizarLaRespuestaDelServicioYEstadoNoNuloDelIdDelParche() {
        ListData userResponse = new ListData();

        try {
            actor.should(
                    seeThat("El codigo de respuesta",
                            ResponseCode
                                    .was(), equalTo(HttpStatus.SC_OK))
            );

            actor.should(
                    seeThat("El Id de usuario no debe ser nulo",
                            act -> userResponse.getId(), notNullValue())
            );

        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }
}
