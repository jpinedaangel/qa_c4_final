package co.com.sofka.stepdefinitions.ui.layout;

import co.com.sofka.GeneralSetupUi;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;

import static co.com.sofka.question.ui.LayoutPublicoCrearCuentaQuestion.layoutPublicoCrearCuentaQuestion;
import static co.com.sofka.question.ui.LayoutPublicoInicioDeSesionQuestion.layoutPublicoInicioDeSesionQuestion;
import static co.com.sofka.question.ui.LayoutPublicoInicioQuestion.layoutPublicoInicioQuestion;
import static co.com.sofka.task.ui.OpenLandingPage.openLandingPage;
import static co.com.sofka.task.ui.layout.LayoutPublicoCrearCuenta.layoutPublicoCrearCuenta;
import static co.com.sofka.task.ui.layout.LayoutPublicoInicio.layoutPublicoInicio;
import static co.com.sofka.task.ui.layout.LayoutPublicoInicioDeSesion.layoutPublicInicioDeSesion;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class LayOutPublicoStepdefinition extends GeneralSetupUi {
    private static final Logger LOGGER = Logger.getLogger(LayOutPublicoStepdefinition.class);

    // Escenario 1
    @Dado("que el usuario está en el Layout publico")
    public void queElUsuarioEstáEnElLayoutPublico() {
        try {
        actorSetupTheBrowser(actor.getName());
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage()
        );
        }catch (Exception e){
            LOGGER.info("Error en la apertura de la pagina parches");
        }
    }

    @Cuando("el usuario hace clic en el boton crear cuenta")
    public void elUsuarioHaceClicEnElBotonCrearCuenta() {
        try {
        theActorInTheSpotlight().attemptsTo(
                layoutPublicoCrearCuenta()
        );
        }catch (Exception e){
            LOGGER.info("Error accediendo al layout publico");
        }
    }

    @Entonces("el usuario es redireccionado a la pagina correspondiente a crear cuenta")
    public void elUsuarioEsRedireccionadoALaPaginaCorrespondienteACrearCuenta() {
        try {
        theActorInTheSpotlight().should(
                seeThat(
                        layoutPublicoCrearCuentaQuestion(), equalTo("Registro")
                )
        );
        }catch (Exception e){
            LOGGER.info("Error en el boton crear cuenta");
        }
    }

    // Escenario 2
    @Dado("que el cliente abrio la página del Layout publico")
    public void queElClienteAbrioLaPáginaDelLayoutPublico() {
        try {
        actorSetupTheBrowser(actor.getName());
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage()
        );
        }catch (Exception e){
            LOGGER.info("Error en la apertura de la pagina parches");
        }
    }

    @Cuando("el usuario hace clic en el boton inicio de sesion")
    public void elUsuarioHaceClicEnElBotonInicioDeSesion() {
        try {
        theActorInTheSpotlight().attemptsTo(
                layoutPublicInicioDeSesion()
        );
        }catch (Exception e){
            LOGGER.info("Error en accediendo al layout publico");
        }
    }

    @Entonces("el usuario es redireccionado a la pagina correspondiente de inicio de sesion")
    public void elUsuarioEsRedireccionadoALaPaginaCorrespondienteDeInicioDeSesion() {
        try {
        theActorInTheSpotlight().should(
                seeThat(
                        layoutPublicoInicioDeSesionQuestion(), equalTo("Inicio de Sesión")
                )
        );}catch (Exception e){
            LOGGER.info("Error en el boton incio de seison");
        }
    }

    // Escenario 3

    @Dado("que el cliente accedio al sitio del Layout publico")
    public void queElClienteAccedioAlSitioDelLayoutPublico() {
        try {
        actorSetupTheBrowser(actor.getName());
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage()
        );
        }catch (Exception e){
            LOGGER.info("Error en la apertura de la pagina parches");
        }
    }

    @Cuando("el cliente hace clic en el boton inicio")
    public void elClienteHaceClicEnElBotonInicio() {
        try {
        theActorInTheSpotlight().attemptsTo(
                layoutPublicoCrearCuenta(),
                layoutPublicoInicio()
        );
        }catch (Exception e){
            LOGGER.info("Error accediendo al layout publico");
        }
    }

    @Entonces("el cliente es redireccionado a la pagina correspondiente de inicio")
    public void elclienteEsRedireccionadoALaPaginaCorrespondienteDeInicioDeSesion() {
        try {
        theActorInTheSpotlight().should(
                seeThat(
                        layoutPublicoInicioQuestion(), equalTo("Descripción")
                )
        ); }catch (Exception e){
            LOGGER.info("Error en el boton inicio");
        }
    }

}
